const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 3001;


// [SECTION] MongoDB connection

// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
mongoose.connect("mongodb+srv://Tristan_224:admin123@batch-224-vasco.lmcsvxb.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

// Connecting to MongoDB locally
let db = mongoose.connection;
// If a connection error occured, output in the console
// "connection error" is the message that will display if an error occured
db.on("error", console.error.bind(console, "connection error"));
// If the connection is successful, output in  the console
db.once("open", () => console.log("Connected to MongoDB"));


// Schemas determine the structure of the documents to be written in the database
// "new" creates a new Schema
const userSchema = new mongoose.Schema({
	// Define ng fields with the corresponding data type
	username: String,
	password: String
});

// Models
const User = mongoose.model("User", userSchema);

// [SECTION] Creation of todo list routes
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post("/signup", (req, res) => {
	// "findOne" is a mongoose method that acts similar to "find" of MongoDB
	User.findOne({username: req.body.username, password: req.body.password}, (err, result) => {

		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.username == req.body.username && result.password == req.body.password){
			// Return message to the client/Postman
			return res.send("Duplicate task found!")
		// If no matching document
		}else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});
			// "save" method will store the information to the database
			newUser.save((saveErr, savedUser) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				// No error found while creating the document
				} else {
					// indication that a new task was created
					return res.status(201).send("New User Registered");
				}
			})
		}
	})
});





app.listen(port, () => console.log(`Server running at port ${port}`));